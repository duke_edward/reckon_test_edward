import express from "express";
import { Routes } from "./routes/route";

class App {
  public app: express.Application = express();
  public routePrv: Routes = new Routes();

  constructor() {
    this.config();
    this.routePrv.routes(this.app);
  }

  private config(): void {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
  }
}

export default new App().app;
