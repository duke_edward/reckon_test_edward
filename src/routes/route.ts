import { Request, Response, NextFunction } from "express";
import { AlgorithmController } from "../controllers/algorithmController";
import { DivisorController } from "../controllers/divisorController";
import { StringMatchController } from "../controllers/stringMatchController";

export class Routes {
  public divisorController: DivisorController = new DivisorController();
  public stringMatchController: StringMatchController =
    new StringMatchController();
  public algorithmController: AlgorithmController = new AlgorithmController();

  public routes(app): void {
    app.route("/").get((req: Request, res: Response) => {
      res.status(200).send({
        message: "NodeJs Pre-Interview Coding Test",
      });
    });

    // NOTE: /divisors - Divisor (Set value: from Endpoint 2)
    app.route("/divisors").get(this.divisorController.getDivisor);

    // NOTE: /matches - StringMatch (Default: BT: Brute Force)
    app.route("/matches").get(this.stringMatchController.getStringMatchCode);

    // NOTE: /matches/{algorithm_code}
    app
      .route("/matches/:algorithm_code")
      .get(this.stringMatchController.getStringMatchCode);

    // NOTE: /Algorithms
    app.route("/algorithms").get(this.algorithmController.getAlgorithms);
  }
}
