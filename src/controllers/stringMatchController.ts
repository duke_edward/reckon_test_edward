import { Request, Response } from "express";
import * as matchModule from "../modules/stringMatchModule";
import * as reckonModule from "../modules/reckonModule";

export class StringMatchController {
  public async getStringMatchCode(req: Request, res: Response) {
    let algorithm_code = "BF";

    if (req.params.algorithm_code === "KMP") {
      algorithm_code = req.params.algorithm_code;
    }

    const text_data = await reckonModule.getTextString();
    const pattern_data = await reckonModule.getSubTexts();
    const finalObj = await matchModule.stringMatching(
      algorithm_code,
      text_data,
      pattern_data
    );
    const result = await reckonModule.postSubmitResults(finalObj);

    res.status(200);
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(result));
  }
}
