import { Request, Response } from "express";
import * as jsonData from "../jsons/algorithms.json";

export class AlgorithmController {
  public getAlgorithms(req: Request, res: Response) {
    res.status(200);
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(jsonData.algorithms));
  }
}
