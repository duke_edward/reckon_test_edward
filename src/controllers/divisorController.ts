import { Request, Response } from "express";
import * as divisorModule from "../modules/divisorModule";
import * as reckonModule from "../modules/reckonModule";

export class DivisorController {
  public async getDivisor(req: Request, res: Response) {
    const range_data = await reckonModule.getRange();
    const divisor_data = await reckonModule.getDivisors();
    const result = await divisorModule.divisorLoop(range_data, divisor_data);

    res.status(200);
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(result));
  }
}
