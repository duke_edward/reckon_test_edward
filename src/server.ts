import app from "./app";
import * as dotenv from "dotenv";

dotenv.config();

// NOTE: Server Listening Port Number
const PORT = process.env["RK_SERVER_PORT"];

app.listen(PORT, () => {
  console.log("Express server listening on port " + PORT);
});
