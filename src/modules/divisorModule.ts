interface divisor_obj {
  key: number;
  value: string;
}

function checkDivisor(current_number: number, divisor_data: any): string {
  let result = "";
  let divisor_data_size = divisor_data.outputDetails.length;

  if (divisor_data_size > 0) {
    for (let i = 0; i < divisor_data_size; i++) {
      let divisor = divisor_data.outputDetails[i].divisor;
      let output = divisor_data.outputDetails[i].output;

      if (current_number % divisor == 0) {
        result = result + output;
      }
    }
  }

  return result;
}

export const divisorLoop = async (range_data: any, divisor_data: any) => {
  let i_lower = range_data.lower;
  let i_upper = range_data.upper;
  let resultArray = [];

  for (let i = i_lower; i <= i_upper; i++) {
    if (i > 0) {
      console.log(i + ": " + checkDivisor(i, divisor_data));

      let divisorObj = {} as divisor_obj;
      divisorObj.key = i;
      divisorObj.value = checkDivisor(i, divisor_data);

      resultArray.push(divisorObj);
    }
  }

  return resultArray;
};
