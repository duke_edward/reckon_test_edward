interface final_obj {
  candidate: string;
  text: any;
  results: any;
}

interface result_obj {
  subtext: string;
  result: any;
}

function bruteForceStringMatch(text_data: string, pattern_data: string) {
  let text_size: number = text_data.length;
  let pattern_size: number = pattern_data.length;

  let resultArray = [];

  for (let i = 0; i <= text_size - pattern_size; i++) {
    let finded: boolean = true;

    for (let j = 0; j < pattern_size; j++) {
      if (text_data[i + j] != pattern_data[j]) {
        finded = false;
        break;
      }
    }

    if (finded) {
      resultArray.push(i + 1);
      finded = true;
    }
  }

  return resultArray;
}

function knuthMorrisPrattStringMatch(text_data: string, pattern_data: string) {
  let text_size: number = text_data.length;
  let pattern_size: number = pattern_data.length;

  let resultArray = [];

  for (let i = 0; i <= text_size - pattern_size; i++) {
    let finded: boolean = true;

    for (let j = 0; j < pattern_size; j++) {
      if (text_data[i + j] != pattern_data[j]) {
        finded = false;
        break;
      }
    }

    if (finded) {
      resultArray.push(i + 1);
      finded = true;
    }
  }

  return resultArray;
}

export const stringMatching = async (
  algorithm_code: string,
  text_data: any,
  pattern_data: any
) => {
  let pattern_data_size = pattern_data.subTexts.length;
  let resultArray = [];
  let finalObj = {} as final_obj;

  if (pattern_data_size > 0) {
    for (let i = 0; i < pattern_data_size; i++) {
      let param_text_data = text_data.text.toUpperCase();
      let param_pattern_data = pattern_data.subTexts[i].toUpperCase();

      let result = bruteForceStringMatch(param_text_data, param_pattern_data);

      let resultObj = {} as result_obj;
      resultObj.subtext = pattern_data.subTexts[i];
      if (result.length > 0) {
        resultObj.result = result.toString();
      } else {
        resultObj.result = "<No Output>";
      }

      resultArray.push(resultObj);
    }
  }

  finalObj.candidate = "Edward";
  finalObj.text = text_data.text;
  finalObj.results = resultArray;

  console.log(finalObj);

  return finalObj;
};
