import axios, { AxiosResponse } from "axios";
import * as range_json_data from "../jsons/taks_1_endpoint_1.json";
import * as divisor_json_data from "../jsons/taks_1_endpoint_2.json";
import * as text_json_data from "../jsons/taks_2_endpoint_1.json";
import * as sub_text_json_data from "../jsons/taks_2_endpoint_2.json";

export const getRange = async () => {
  try {
    const range_url = "https://join.reckon.com/test1/rangeInfo";
    const response = await axios.get(range_url);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    // console.error(err)
    return range_json_data;
  }
};

export const getDivisors = async () => {
  try {
    const range_url = "https://join.reckon.com/test1/divisorInfo";
    const response = await axios.get(range_url);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    // console.error(err)
    return divisor_json_data;
  }
};

export const getTextString = async () => {
  try {
    const range_url = "https://join.reckon.com/test2/textToSearch";
    const response = await axios.get(range_url);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    // console.error(err)
    return text_json_data;
  }
};

export const getSubTexts = async () => {
  try {
    const range_url = "https://join.reckon.com/test2/subTexts";
    const response = await axios.get(range_url);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    return sub_text_json_data;
  }
};

export const postSubmitResults = async (payload) => {
  try {
    const range_url = "https://join.reckon.com/test2/submitResults";
    const response = await axios.post(range_url, payload);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    return sub_text_json_data;
  }
};
