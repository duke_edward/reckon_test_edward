# README #

This README would normally document whatever steps are necessary to get your application up and running.

# Pre-reqs
To build and run this app locally you will need a few things:
* Install [Node.js](https://nodejs.org/en/)
* Install [Yarn](https://yarnpkg.com/)
* Install [VS Code](https://code.visualstudio.com/)

Versions:
* Node Version [v14.xx.x](https://nodejs.org/en/)
* Yarn Version [v1.x.x](https://yarnpkg.com/)
# Getting started
- Clone the repository
```
git clone https://duke_edward@bitbucket.org/duke_edward/reckon_test_edward.git
```
- Install dependencies
```
cd <project_name>
npm install
```
- Build and run the project
```
yarn start
```

# REST APIs
- Test 1. divisors
```
http://localhost:9999/divisors
```
- Test 2. matches
```
http://localhost:9999/matches
```
or
```
http://localhost:9999/matches/{algorithm_code}
```

# Postman Collection
- File Path
```
/assets/postman_collections
```
### How to import a collection into Postman ###

* To open the Postman application, click on its icon on the taskbar.
* Click on the file tab and then click import.
* Choose the method you want to import an item.
* Choose the correct item to import and press open. Postman will automatically import the item.